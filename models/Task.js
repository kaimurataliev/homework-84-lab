const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TaskSchema = new Schema ({
    user: {
        type: Schema.Types.ObjectId, ref: 'User', required: true,
    },
    title: {
        String, required: true
    },
    description: {
        type: String
    },
    status: {
        type: String, default: "new"
    }
});


const Task = mongoose.model('Task', TaskSchema);

module.exports = Task;
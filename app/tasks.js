const express = require('express');
const Task = require('../models/Task');
const auth = require('../auth');
const router = express.Router();
const nanoid = require('nanoid');

const config = require('../config');

const createRouter = () => {
    //Task get
    router.get('/', auth, async (req, res) => {
        const tasks = await Task.find({user: req.user});

        res.send(tasks);
    });

    // Task post
    router.post('/', auth, (req, res) => {
        const taskData = req.body;

        const task = new Task(taskData);

        task.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;
